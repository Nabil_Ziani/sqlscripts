USE ModernWays;

SET SQL_SAFE_UPDATES = 0;
UPDATE AuteursBoeken
SET Titel =  "Pet Cemetery" WHERE Titel = "Pet Sematary";
SET SQL_SAFE_UPDATES = 1;

-- Dit werkt niet want de column is gemaakt met agregaatfunctie (concat) --
SET SQL_SAFE_UPDATES = 0;
UPDATE AuteursBoeken
SET Auteur = "Steven King" WHERE Titel = "Stephen King";
SET SQL_SAFE_UPDATES = 1;