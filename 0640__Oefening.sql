USE `aptunes`;
DROP procedure IF EXISTS `GetLiedjes`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `GetLiedjes`(IN woord VARCHAR(50))
BEGIN
SELECT Titel FROM Liedjes 
WHERE Titel LIKE concat('%', woord, '%') ;
END$$

DELIMITER ;