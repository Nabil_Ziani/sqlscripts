USE `aptunes`;
DROP procedure IF EXISTS `GetAlbumDuration2`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `GetAlbumDuration2` (IN numberAlbum INT, OUT totalDuration SMALLINT UNSIGNED)
SQL SECURITY INVOKER
BEGIN
	DECLARE songDuration TINYINT UNSIGNED DEFAULT(0);
	DECLARE ok BOOL DEFAULT FALSE;

	DECLARE songDurationCursor CURSOR FOR SELECT Lengte FROM Liedjes WHERE Albums_Id = numberAlbum;
	DECLARE CONTINUE HANDLER FOR NOT FOUND SET ok = TRUE;
	SET totalDuration = 0;

	OPEN songDurationCursor;
		fetchLoop: LOOP
			FETCH songDurationCursor INTO songDuration;
			IF ok = TRUE THEN
				LEAVE fetchLoop;
			END IF;
			SET totalDuration = totalDuration + songDuration;
		END LOOP fetchLoop;
	CLOSE songDurationCursor;
END$$

DELIMITER ;