USE aptunes; 

CREATE USER IF NOT EXISTS student IDENTIFIED BY 'ikbeneenstudent';
GRANT EXECUTE ON PROCEDURE aptunes.GetAlbumDuration TO student; 