USE ModernWays;

SELECT 
	Games.Titel, 
	COALESCE(Platformen.Naam, 'Geen platformen gekend') AS Naam
FROM Games
LEFT JOIN Releases ON Games.Id = Releases.Games_Id
-- Vanaf hier wordt de gewenste data opgevraagd -- 
LEFT JOIN Platformen ON Platformen.Id = Releases.Platformen_Id
WHERE Releases.Platformen_Id IS NULL
UNION -- Met UNION kunnen we 2 tabellen "samensmelten"
SELECT 
	COALESCE(Games.Titel, 'Geen games gekend') AS Titel,
    Platformen.Naam
FROM Platformen
LEFT JOIN Releases ON Platformen.Id = Releases.Platformen_Id
-- Vanaf hier wordt de gewenste data opgevraagd -- 
LEFT JOIN Games ON Games.Id = Releases.Games_Id
WHERE Releases.Games_Id IS NULL