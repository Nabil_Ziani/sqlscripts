USE ModernWays;

SELECT Platformen.Naam, COUNT(Platformen.Naam) AS 'Aantal games beschikbaar'
FROM Releases
INNER JOIN Platformen ON Releases.Platformen_Id = Platformen.Id
GROUP BY Platformen.Naam