USE ModernWays;

-- Relatietabel tussen leden en boeken
CREATE TABLE Uitleningen (
	StartDatum DATE NOT NULL,
    EindDatum DATE,
    -- De 2 foreign keys --
	Leden_Id INT NOT NULL,
    Boeken_Id INT NOT NULL,
    -- Koppeling maken -- 
    CONSTRAINT fk_Uitleningen_Leden FOREIGN KEY (Leden_Id) REFERENCES Leden(Id),
    CONSTRAINT fk_Uitleningen_Boeken FOREIGN KEY (Boeken_Id) REFERENCES Boeken(Id)
);