USE ModernWays;
ALTER TABLE Liedjes ADD COLUMN Genre VARCHAR(20) CHARSET utf8mb4;

SET SQL_SAFE_UPDATES = 0;
UPDATE liedjes 
SET genre = 'Hard Rock' 
WHERE Artiest = 'Led Zeppelin' or Artiest = 'Van Halen';
SET SQL_SAFE_UPDATES = 1;