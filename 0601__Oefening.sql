USE ModernWays;

INSERT INTO BoekenNaarAuteurs (Boeken_Id, Auteurs_Id)
-- 10 = Haruki Murakami (Auteur)
-- 1 = Norwegian Wood (Boek)
-- 2 = Kafka on the shore (Boek)
-- ...
VALUES 
(1, 10),
(2, 10),
(3, 16),
(4, 16),
(5, 17),
(6, 18),
(6, 16),
(7, 17),
(7, 19);

-- Je kan het ook op deze manier inserten: 
/* SELECT Boeken.Id, Auteurs.Id
FROM Boeken CROSS JOIN Auteurs 
WHERE Boeken.Titel = "Norwegian Wood"
AND Auteurs.Voornaam = "Haruki" 
AND Auteurs.Familienaam = "Murakami"
OR Boeken.Titel = "Kafka on the shore"
AND Auteurs.Voornaam = "Haruki" 
AND Auteurs.Familienaam = "Murakami" */
