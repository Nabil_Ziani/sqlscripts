USE ModernWays;

INSERT INTO Uitleningen (StartDatum, EindDatum, Leden_Id, Boeken_Id) 
VALUES
-- Begin- en einddatum, Max = Id 3, Norwegian Wood = Id 1
('2019-2-1', '2019-2-15', 3, 1),
-- de rest...
('2019-2-16', '2019-3-2', 2, 1),
('2019-2-16', '2019-3-2', 2, 5),
('2019-5-1', NULL, 1, 5);