USE ModernWays;
SELECT LeeftijdCategorie, AVG(Loon) AS 'Gemiddeld loon' 
FROM  
(SELECT Id, FLOOR(Leeftijd / 10) * 10 AS 'Leeftijdscategorie'
FROM Personeelsleden) AS LeeftijdCategorie
INNER JOIN Personeelsleden ON Personeelsleden.Id = Leeftijdcategorie.Id
GROUP BY Leeftijdcategorie;