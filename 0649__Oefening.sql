USE `aptunes`;
DROP procedure IF EXISTS `DemonstrateHandlerOrder`;

DELIMITER $$
USE `aptunes`$$
CREATE PROCEDURE `DemonstrateHandlerOrder` ()
BEGIN
DECLARE getal INT DEFAULT(0);
-- Error Handlers --
DECLARE EXIT HANDLER FOR SQLEXCEPTION 
BEGIN
	RESIGNAL SET MESSAGE_TEXT = 'Ik heb mijn best gedaan!';
END;
DECLARE CONTINUE HANDLER FOR SQLSTATE '45002'
BEGIN
	SELECT "State 45002 opgevangen. Geen probleem." Message; 
END;

SET getal =  FLOOR(RAND() * 3) + 1;
IF getal = 1 THEN
	SIGNAL SQLSTATE '45001';
ELSEIF getal = 2 THEN 
	SIGNAL SQLSTATE '45002';
ELSE
	SIGNAL SQLSTATE '45003';
END IF;
    
END$$
DELIMITER ;