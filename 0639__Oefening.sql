USE ModernWays;

-- Nieuwe auteur inserten
INSERT INTO Personen (
	Voornaam, Familienaam
)
VALUES (
	'Jean-Paul', 'Sartre'
);

-- Een boek van de nieuwe auteur inserten
INSERT INTO Boeken (
   Titel,
   Stad,
   Verschijningsdatum,
   Commentaar,
   Categorie,
   Personen_Id
)
values (
   'De Woorden',
   'Antwerpen',
   '1962',
   'Een zeer mooi boek.',
   'Roman',
   (SELECT Id FROM Personen WHERE
	Familienaam = 'Sartre' and Voornaam = 'Jean-Paul')
);