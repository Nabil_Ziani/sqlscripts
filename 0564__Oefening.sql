USE Hogeschool;

CREATE TABLE Lector(
Familienaam VARCHAR(100),
Voornaam VARCHAR(100),
Personeelsnummer INT AUTO_INCREMENT PRIMARY KEY
);

CREATE TABLE Vak(
Naam VARCHAR(100) PRIMARY KEY
);

CREATE TABLE Opleiding(
Naam VARCHAR(100) PRIMARY KEY
);

CREATE TABLE Student(
Familienaam VARCHAR(100),
Voornaam VARCHAR(100),
Studentennummer INT AUTO_INCREMENT PRIMARY KEY,
Semester VARCHAR(100),
-- Relatie naar Opleiding --
Opleiding_Naam VARCHAR(100),
CONSTRAINT fk_Student_Opleiding FOREIGN KEY(Opleiding_Naam) REFERENCES Opleiding(Naam)
);

-- Relatietabel tussen Lector en Vak --
CREATE TABLE Geeft(
Lector_Personeelsnummer INT NOT NULL,
Vak_Naam VARCHAR(100) NOT NULL,
CONSTRAINT fk_Geeft_Lector FOREIGN KEY (Lector_Personeelsnummer) REFERENCES Lector(Personeelsnummer),
CONSTRAINT fk_Geeft_Vak FOREIGN KEY (Vak_Naam) REFERENCES Vak(Naam)
);

-- Relatietabel tussen Vak en Opleiding -- 
CREATE TABLE Opleidingsonderdeel(
Semester TINYINT UNSIGNED,
Vak_Naam VARCHAR(100) NOT NULL,
Opleiding_Naam VARCHAR(100) NOT NULL,
CONSTRAINT fk_Opleidingsonderdeel_Vak FOREIGN KEY(Vak_Naam) REFERENCES Vak(Naam),
CONSTRAINT fk_Opleidingsonderdeel_Opleiding FOREIGN KEY(Opleiding_Naam) REFERENCES Opleiding(Naam)
);