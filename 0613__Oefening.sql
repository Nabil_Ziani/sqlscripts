USE ModernWays;

CREATE VIEW AuteursBoeken AS
SELECT concat(Personen.Voornaam, " ", Personen.Familienaam) AS Auteur, Boeken.Titel FROM Publicaties
INNER JOIN Personen ON Personen.Id = Publicaties.Personen_Id
INNER JOIN Boeken ON Boeken.Id = Publicaties.Boeken_Id