USE ModernWays;

SELECT Leden.Voornaam, Boeken.Titel
FROM Uitleningen 
INNER JOIN Leden ON Leden_Id = Leden.Id
INNER JOIN Boeken ON Boeken_Id = Boeken.Id