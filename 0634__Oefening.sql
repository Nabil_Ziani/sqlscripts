USE ModernWays;
SELECT MIN(PuntenGemiddelde) AS 'Gemiddelde' FROM  
-- Subquery geeft kolom met het puntengemiddelde van alle studenten --
(SELECT AVG(Cijfer) AS PuntenGemiddelde
 FROM Evaluaties
 GROUP BY Studenten_Id) AS GemiddeldePunten;