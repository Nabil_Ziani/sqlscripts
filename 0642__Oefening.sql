USE `aptunes`;
DROP procedure IF EXISTS `CleanupOldMemberships`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `CleanupOldMemberships`(IN someDate DATE, OUT numberCleaned INT)
BEGIN
START TRANSACTION;
	SET numberCleaned = (SELECT COUNT(*) FROM Lidmaatschappen 
	WHERE Einddatum IS NOT NULL AND Einddatum < someDate);

	SET SQL_SAFE_UPDATES = 0;
	DELETE FROM Lidmaatschappen
	WHERE Einddatum IS NOT NULL AND Einddatum < someDate;
	SET SQL_SAFE_UPDATES = 1;
COMMIT;
END$$

DELIMITER ; 