USE ModernWays;
ALTER TABLE huisdieren ADD COLUMN Geluid VARCHAR(20) CHARSET utf8mb4;

SET SQL_SAFE_UPDATES = 0;
UPDATE huisdieren SET Geluid = 'Waf!' WHERE Soort = 'Hond';
UPDATE huisdieren SET Geluid = 'Miauwww...' WHERE Soort = 'Kat';
SET SQL_SAFE_UPDATES = 1;
