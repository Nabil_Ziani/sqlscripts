USE `aptunes`;
DROP procedure IF EXISTS `NumberOfGenres`;

DELIMITER $$
USE `aptunes`$$
CREATE DEFINER=`root`@`localhost` PROCEDURE `NumberOfGenres`(OUT aantal TINYINT)
BEGIN
SELECT COUNT(*) INTO aantal  FROM Genres ;
END$$

DELIMITER ;